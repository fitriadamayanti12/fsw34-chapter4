class Human {
  constructor(props) {
    if (this.constructor === Human) {
      throw new Error("Cannot instantiate from Abstract Class");
      // it's abstract
    }

    let { name, address } = props;
    this.name = name;
    this.address = address;
    this.profession = this.constructor.name; // Every human has profession, and let child class to define it.
  }

  work() {
    console.log("working...");
  }

  introduce() {
    console.log(`Hello, my name is ${name}`);
  }
}

class Hokage extends Human {
  constructor(props) {
    super(props);
    this.rank = props.rank; // Add new property, rank.
  }

  work() {
    console.log("Go to the Hokage Palace");
    super.work();
  }
}

const Naruto = new Hokage({
    name: "Naruto",
    address: "Tanjung Priok",
    rank: "General"
})
console.log(Naruto.profession);

try {
    let Abstract = new Human({
        name: "Abstract",
        address: "Konoha"
    })
} catch (err) {
    console.log(err.message)
}


