// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

/* 
Output: belajar Javascript sangat menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";
/* 
Output: Belajar Javascript sangat menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall

Output:
BMI Steven =
BMI Bill = 
*/

/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/

//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
