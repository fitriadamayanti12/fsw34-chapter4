/* 
1. Do NOT change any of the existing code.
2. You should NOT redeclare the existing variables.
3. Your code only add console.log
*/

let s1 = "Pemrograman JavaScript";
let s2 = "Node.js";
let s3 = `Express (framework untuk Node.js)`;

/* 
Output:
s1: Pemrograman JavaScript
s2: Node.js
s3: Express (framework untuk Node.js) & Node.js
s4: Pemrograman JavaScript & Node.js
*/
