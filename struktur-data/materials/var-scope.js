var diskon = 500; // Global scope
if (true) {
  var diskon = 300; // Global scope
}
console.log(diskon);
// Karena var adalah global scope

/* Sebelum ada ES6, solusinya membuat function scope -> local scope */
var diskon = 500; // Global scope
function diskonScope() {
  var diskon = 300; // Local scope
  console.log(diskon);
}
diskonScope();
console.log(diskon);
