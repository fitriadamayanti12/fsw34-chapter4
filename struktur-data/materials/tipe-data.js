// tipe data string
let name = "Sabrina";
let pesan = "mari belajar javascript";

console.log(`tolong sampaikan ${pesan} kepada ${name}`);

let price = 50;
console.log(typeof price);

// Boolean -> true & false
if (true) {
  console.log("Nice");
}

let biodata = {
  nama: "Sabrina",
  hobi: ["nonton", "belajar", "ngoding"],
  ttl: "1 Juni 2023",
};

let fruit = ["Appel", "Orange", "Manggo"];

console.log(biodata.ttl);
console.log(fruit[2]);

const animals = ["Cat", "Dog", "Rabbit"];
animals.push("Bear");
console.log(animals);

// const animals = ["Cat", "Dog"];
// animals.pop();
// console.log(animals);

// item in items
// object items
// foreach item
// <div foreach={item in items}>

animals.forEach(function (item) {
  console.log(item);
});
