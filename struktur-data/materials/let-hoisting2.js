let message = "Hello";
function greetings() {
  console.log(message);
  let message = "Hello World!";
}
greetings();

/* Setelah kode direvisi */
let message = "Hello";
function greetings() {
  let message = "Hello World!";
  console.log(message);
}
greetings();
