var name; // Declaration
console.log(name);

name = "Bot"; // Assignment
console.log(name);

var name = "Bot Sabrina"; // Redeclared and Reassigned
console.log(name);
