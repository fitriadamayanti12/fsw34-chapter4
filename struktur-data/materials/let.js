/* Buat variabel yang langsung kita kasih nilai */
let pesan = "Hello World";
console.log(pesan);

/* Buat banyak variabel sekaligus */
let nama = "Sabrina", // dipisahkan dengan koma
  umur = 25, // dipisahkan dengan koma
  jenisKelamin = "Perempuan";

console.log(nama);
console.log(umur);
console.log(jenisKelamin);
