let name; // Declaration
console.log(name);

name = "Bot"; // Assignment
console.log(name);

name = "Bot Sabrina"; // Reassigned
console.log(name);

let name = "Mentor Sabrina"; // Can not redeclared
console.log(name);
