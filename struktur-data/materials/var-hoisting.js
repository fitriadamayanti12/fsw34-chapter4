name = "Mentor Sabrina"; // Variabel di-assign duluan
var name; // Kemudian baru dideklarasikan
console.log(name);

/* Di belakang layar terjadi hoisting */
var name;
name = "Mentor Sabrina";
console.log(name);
