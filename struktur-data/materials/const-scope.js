const diskon = 500;
if (true) {
  // Tanda awal scope
  const diskon = 300; // Hanya bisa diakses di dalam scope
  console.log(diskon);
} // Tanda akhir scope
console.log(diskon);
