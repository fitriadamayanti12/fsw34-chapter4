/* Kita gak bisa ubah nilai bumi */
const bumi = "bulat";
bumi = "datar";
// Output: TypeError: invalid assignment to const `bumi'

/* Kita juga gak bisa deklarasi ulang */
const bumi = "datar";
// Output: SyntaxError:Identifier 'bumi' has already been declared

/* Object dengan variabel const masih bisa kita ubah property-nya */
const obj = { id: 1, name: "Sabrina" };
obj.location = "Jakarta";
console.log(obj); // Output: { id:1, name:'Sabrina', location:'Jakarta'}
// Tapi, kita gak bisa reassigned
obj = {}; // Output: TypeError: Assignment to constant variable.

/* Array dengan variabel const masih bisa kita ubah property-nya */
const arr = [1, 2, 3, 4];
arr.push(5);
console.log(arr); // Output: [1,2,3,4,5]
// Tapi, kita gak bisa reassigned
arr = []; // Output: TypeError: Assignment to constant variable.
