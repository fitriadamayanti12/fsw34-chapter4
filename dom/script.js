// selecting elements
console.log(document.documentElement);
console.log(document.head);
console.log(document.body);

const body = document.querySelector("#title");
const allSelections = document.querySelectorAll(".row");
console.log(body);
console.log(allSelections[1]);

console.log(document.getElementById("title"));

const allButtons = document.getElementsByTagName("button");
const buttonName = document.getElementsByClassName("navbar-toggler");
console.log(allButtons);
console.log(allButtons[1]);

console.log(document.getElementsByClassName("container-fluid"));

// Creating and inserting element
const message = document.createElement("div");
message.classList.add("cookies-message");
message.innerHTML = `Thank you for visiting our website 
  <button class='btn btn--close-cookie'>Got it!</button>`;

body.prepend(message);
//body.append(message.cloneNode(true));

//body.after(message);

// Delete elements
document
  .querySelector(".btn--close-cookie")
  .addEventListener("click", function () {
    message.remove();
    //message.parentElement.removeChild(message);
  });

// Styles
message.style.backgroundColor = "#9fc088";
message.style.width = "100%";

console.log(message.style.color);

message.style.height =
  Number.parseFloat(getComputedStyle(message).height, 10) + 40 + "px";

document.documentElement.style.setProperty("carousel-item", "orangered");

// Attributes
const image = document.querySelector(".title-img");
console.log(image.alt);
console.log(image.className);

// Non standar
console.log(image.designer);
image.setAttribute("company", "pet-shop");
console.log(image.src);
console.log(image.getAttribute("src"));

// Data attributes
console.log(image.dataset.versionNumber);

// Classes
image.classList.add("a", "j");
image.classList.remove("a", "j");

// Events & Event Handlers
const h1 = document.querySelector("h1");
// h1.addEventListener("mouseenter", function (e) {
//   alert("addEventListener: Great! You are reading the heading");
// });
const alertH1 = function (e) {
  alert("addEventListener: Great! You are reading the heading");
};

h1.addEventListener("mouseenter", alertH1);

setTimeout(() => h1.removeEventListener("mouseenter", alertH1), 3000);
